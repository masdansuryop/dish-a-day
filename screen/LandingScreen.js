import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  View,
  ScrollView,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

export default function LandingScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Image
        style={styles.backgroundImage}
        source={require("../images/background.png")}
      ></Image>
      <View>
        <Text style={styles.greeting}>Welcome</Text>
      </View>
      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={styles.btnBlock}
          onPress={() => navigation.navigate("Register")}
        >
          <Text style={styles.btnBlockText}>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={styles.btnText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
  },
  greeting: {
    fontFamily: "Roboto",
    fontSize: 48,
    lineHeight: 56,
    color: "white",
    marginTop: 136,
    marginLeft: 34,
  },
  bottomContainer: {
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    backgroundColor: "white",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 35,
    alignItems: "center",
  },
  btnBlock: {
    width: 343,
    paddingVertical: 14,
    alignItems: "center",
    backgroundColor: "#F18843",
    borderRadius: 15,
    marginBottom: 12,
  },
  btnBlockText: {
    fontFamily: "Roboto",
    fontSize: 18,
    color: "white",
  },
  btn: {
    width: 343,
    paddingVertical: 14,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#F18843",
    borderRadius: 15,
  },
  btnText: {
    fontFamily: "Roboto",
    fontSize: 18,
    color: "#F18843",
  },
});
