import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  StatusBar,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

import { AuthContext } from "../Context";

export default function ProfileScreen({ navigation }) {
  const { logout } = React.useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
          <Ionicons name="menu" size={24} color="#4F4F4F" />
        </TouchableOpacity>
        <Image
          style={styles.logo}
          source={require("../images/black-logo.png")}
        ></Image>
        <TouchableOpacity onPress={() => logout()}>
          <Ionicons name="exit-outline" size={24} color="#4F4F4F" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={styles.body}>
          <Image
            style={styles.profileImage}
            source={require("../images/profile.png")}
          ></Image>
          <View style={styles.profileDetails}>
            <Text style={styles.profileName}>Masdan Suryo</Text>
            <Text style={styles.profileAddressAge}>Bandung, Indonesia</Text>
            <Text style={styles.profileAddressAge}>23 years</Text>
          </View>
          <View style={styles.socialMedia}>
            <View style={styles.socialItem}>
              <Ionicons name="logo-instagram" size={40} color="#4F4F4F" />
              <Text style={styles.socalName}>masdan.suryo</Text>
            </View>
            <View style={styles.socialItem}>
              <Ionicons name="logo-facebook" size={40} color="#4F4F4F" />
              <Text style={styles.socalName}>masdansuryop</Text>
            </View>
            <View style={styles.socialItem}>
              <Ionicons name="logo-twitter" size={40} color="#4F4F4F" />
              <Text style={styles.socialName}>masdan.suryo</Text>
            </View>
          </View>
          <View style={styles.skillContainer}>
            <Text style={styles.skillTitle}>Skill</Text>
            <View style={styles.skillItem}>
              <Image
                style={styles.skillImage}
                source={require("../images/js-logo.png")}
              ></Image>
              <View style={styles.skillDescription}>
                <Text style={styles.skillName}>Intermediate Javascript</Text>
                <Text style={styles.skillType}>Bahasa Pemrograman</Text>
                <View style={styles.progressBar}>
                  <View style={styles.progress}></View>
                </View>
                <Text style={styles.skillProgress}>70%</Text>
              </View>
            </View>
            <View style={styles.skillItem}>
              <Image
                style={styles.skillImage}
                source={require("../images/react-logo.png")}
              ></Image>
              <View style={styles.skillDescription}>
                <Text style={styles.skillName}>Basic React</Text>
                <Text style={styles.skillType}>Framework</Text>
                <View style={styles.progressBar}>
                  <View style={styles.progress}></View>
                </View>
                <Text style={styles.skillProgress}>80%</Text>
              </View>
            </View>
            <View style={styles.skillItem}>
              <Image
                style={styles.skillImage}
                source={require("../images/js-logo.png")}
              ></Image>
              <View style={styles.skillDescription}>
                <Text style={styles.skillName}>Intermediate Javascript</Text>
                <Text style={styles.skillType}>Bahasa Pemrograman</Text>
                <View style={styles.progressBar}>
                  <View style={styles.progress}></View>
                </View>
                <Text style={styles.skillProgress}>70%</Text>
              </View>
            </View>
            <View style={styles.skillItem}>
              <Image
                style={styles.skillImage}
                source={require("../images/react-logo.png")}
              ></Image>
              <View style={styles.skillDescription}>
                <Text style={styles.skillName}>Basic React</Text>
                <Text style={styles.skillType}>Framework</Text>
                <View style={styles.progressBar}>
                  <View style={styles.progress}></View>
                </View>
                <Text style={styles.skillProgress}>80%</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.tabBar}>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigation.navigate("Home")}
        >
          <Ionicons name="home-outline" size={26} color="#828282" />
          <Text style={styles.tabTitle}>Discover</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Ionicons name="search" size={26} color="#828282" />
          <Text style={styles.tabTitle}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigation.navigate("Profile")}
        >
          <AntDesign name="user" size={24} color="#828282" />
          <Text style={styles.tabTitle}>Profile</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  navBar: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 18,
    justifyContent: "space-between",
  },
  logo: {
    width: 90.1,
    height: 24,
  },
  body: {
    marginHorizontal: 25,
    marginBottom: 25,
  },
  profileImage: {
    height: 370,
    width: 395,
    position: "absolute",
    top: 0,
    left: -25,
  },
  profileDetails: {
    marginTop: 247,
  },
  profileName: {
    color: "#FFFFFF",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    lineHeight: 28,
    marginBottom: 5,
  },
  profileAddressAge: {
    color: "#FFFFFF",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
  },
  socialMedia: {
    width: 325,
    height: 80,
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    marginTop: 21,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  socialItem: {
    flexDirection: "column",
    alignItems: "center",
  },
  socialName: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
  },
  skillContainer: {
    marginTop: 23,
  },
  skillTitle: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 18,
    lineHeight: 21,
  },
  skillItem: {
    flexDirection: "row",
    width: 325,
    height: 80,
    borderRadius: 10,
    marginTop: 18,
    backgroundColor: "#FFFFFF",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  skillImage: {
    width: 80,
    height: 80,
  },
  skillDescription: {
    flexDirection: "column",
    paddingVertical: 7,
    paddingHorizontal: 14,
  },
  skillName: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontSize: 18,
    lineHeight: 21,
  },
  skillType: {
    color: "#828282",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
    marginTop: 6,
  },
  progressBar: {
    width: 200,
    height: 6,
    borderWidth: 0.5,
    borderColor: "#BDBDBD",
    marginVertical: 5,
  },
  progress: {
    width: 160,
    height: 5,
    backgroundColor: "green",
  },
  skillProgress: {
    color: "#BDBDBD",
    fontFamily: "Roboto",
    fontSize: 10,
    lineHeight: 12,
  },
  tabBar: {
    height: 80,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "white",
    borderRadius: 10,
    borderTopWidth: 0.1,
    borderColor: "#828282",
  },
  tabItem: {
    flexDirection: "column",
    alignItems: "center",
  },
  tabTitle: {
    color: "#828282",
    fontFamily: "Roboto",
    fontSize: 12,
    marginTop: 8,
  },
});
