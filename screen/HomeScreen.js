import React, { useContext, useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  ActivityIndicator,
  StatusBar,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

import { AuthContext } from "../Context";
import Axios from "axios";

export default function HomeScreen({ navigation, data }) {
  const { logout } = useContext(AuthContext);

  console.log(data);

  const [recipes, setRecipes] = useState({
    data: {},
    isLoading: true,
    isError: false,
  });

  const API_KEY = "050afbcf5aa64150a0d0c3022dc15e81";

  useEffect(() => {
    getRecipes();
  }, []);

  const getRecipes = async () => {
    try {
      const response = await Axios.get(
        `https://api.spoonacular.com/recipes/random?apiKey=${API_KEY}&number=5`
      );
      setRecipes({
        data: response.data.recipes,
        isLoading: false,
        isError: false,
      });
      console.log(response.data.recipes);
    } catch (error) {
      setRecipes({ isLoading: false, isError: true });
    }
  };

  if (recipes.isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <Image
          style={styles.backgroundImage}
          source={require("../images/splash.png")}
        ></Image>
        <ActivityIndicator size="large" color="#FFFFFF" />
      </View>
    );
  } else if (recipes.isError) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.error}>Terjadi error saat memuat data!</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.navBar}>
        <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
          <Ionicons name="menu" size={24} color="#4F4F4F" />
        </TouchableOpacity>
        <Image
          style={styles.logo}
          source={require("../images/black-logo.png")}
        ></Image>
        <TouchableOpacity onPress={() => logout()}>
          <Ionicons name="exit-outline" size={24} color="#4F4F4F" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={styles.body}>
          <Text style={styles.title}>Hello, Masdan!</Text>
          <Text style={styles.subTitle}>What do you want to cock today?</Text>
          <View style={styles.categoriesContainer}>
            <Text style={styles.categoriesTitle}>Categories</Text>
            <ScrollView style={styles.categoriesSlider} horizontal>
              <TouchableOpacity style={styles.categoriesItem}>
                <Image
                  style={styles.categoriesImage}
                  source={require("../images/categories1.png")}
                ></Image>
                <Text style={styles.categoriesName}>Salads</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.categoriesItem}>
                <Image
                  style={styles.categoriesImage}
                  source={require("../images/categories2.png")}
                ></Image>
                <Text style={styles.categoriesName}>Soups</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.categoriesItem}>
                <Image
                  style={styles.categoriesImage}
                  source={require("../images/categories3.png")}
                ></Image>
                <Text style={styles.categoriesName}>Fish</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.categoriesItem}>
                <Image
                  style={styles.categoriesImage}
                  source={require("../images/categories4.png")}
                ></Image>
                <Text style={styles.categoriesName}>Meat</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <FlatList
            data={recipes.data}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.slider}
                onPress={() => navigation.navigate("Details", { data: item })}
              >
                <Image
                  style={styles.productImage}
                  source={{
                    uri: item.image,
                  }}
                ></Image>
                <View style={styles.descContainer}>
                  <View style={styles.loveContainer}>
                    <Ionicons name="heart" size={24} color="red" />
                  </View>
                  <Text style={styles.productName}>
                    {item.title.substring(0, 20)}
                  </Text>
                  <Text style={styles.productDescription}>
                    {item.summary.substring(0, 60)}
                  </Text>
                  <View style={styles.productDetails}>
                    <Text style={styles.productDuration}>
                      {item.readyInMinutes} MIN
                    </Text>
                    <Text style={styles.productLevel}>{item.dishTypes[0]}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={(item) => item.id.toString()}
            pagingEnabled
            // horizontal={true}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </ScrollView>
      <View style={styles.tabBar}>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigation.navigate("Home")}
        >
          <Ionicons name="home-outline" size={26} color="#828282" />
          <Text style={styles.tabTitle}>Discover</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Ionicons name="search" size={26} color="#828282" />
          <Text style={styles.tabTitle}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigation.navigate("Profile")}
        >
          <AntDesign name="user" size={24} color="#828282" />
          <Text style={styles.tabTitle}>Profile</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  navBar: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 18,
    justifyContent: "space-between",
  },
  logo: {
    width: 90.1,
    height: 24,
  },
  body: {
    marginHorizontal: 25,
    marginBottom: 25,
  },
  title: {
    fontFamily: "Roboto",
    fontSize: 24,
    fontWeight: "700",
    lineHeight: 28.13,
    color: "#4F4F4F",
    marginTop: 60,
    marginLeft: 9,
  },
  subTitle: {
    marginTop: 13,
    marginLeft: 9,
    fontFamily: "Roboto",
    fontSize: 14,
    lineHeight: 16,
    color: "#F18843",
  },
  slider: {
    width: 325,
    height: 220,
    justifyContent: "center",
    marginBottom: 65,
  },
  productImage: {
    width: "100%",
    height: "100%",
    borderRadius: 20,
    marginTop: 37,
  },
  descContainer: {
    position: "absolute",
    top: 158,
    left: 15,
    paddingHorizontal: 18,
    paddingVertical: 15,
    backgroundColor: "white",
    alignItems: "center",
    borderRadius: 20,
    width: 295,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  productName: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    lineHeight: 28,
  },
  productDescription: {
    color: "#BDBDBD",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
    marginTop: 12,
    textAlign: "center",
  },
  loveContainer: {
    position: "absolute",
    top: -20,
    right: 22,
    backgroundColor: "white",
    borderRadius: 50,
    padding: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  productDetails: {
    flexDirection: "row",
    width: 200,
    justifyContent: "center",
    marginTop: 19,
  },
  productDuration: {
    borderRightWidth: 0.5,
    borderColor: "black",
    paddingRight: 25,
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 14,
    lineHeight: 16,
  },
  productLevel: {
    paddingLeft: 25,
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 14,
    lineHeight: 16,
  },
  categoriesContainer: {
    marginTop: 20,
  },
  categoriesTitle: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 18,
    lineHeight: 21,
    marginBottom: 8,
  },
  categoriesSlider: {
    flex: 1,
    flexDirection: "row",
    marginBottom: 12,
  },
  categoriesItem: {
    flexDirection: "column",
    alignItems: "center",
    marginRight: 13,
  },
  categoriesImage: {
    width: 100,
    height: 100,
    borderRadius: 5,
  },
  categoriesName: {
    marginTop: 8,
    color: "#828282",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
  },
  tabBarConainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
  },
  tabBar: {
    height: 80,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "white",
    borderRadius: 10,
    borderTopWidth: 0.1,
    borderColor: "#828282",
  },
  tabItem: {
    flexDirection: "column",
    alignItems: "center",
  },
  tabTitle: {
    color: "#828282",
    fontFamily: "Roboto",
    fontSize: 12,
    marginTop: 8,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  error: {
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: "700",
    color: "#4F4F4F",
  },
  backgroundImage: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
  },
});
