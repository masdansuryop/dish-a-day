import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

import { AuthContext } from "../Context";

export default function RegisterScreen({ navigation }) {
  const { login } = React.useContext(AuthContext);
  return (
    <View style={styles.container}>
      <Image
        style={styles.backgroundImage}
        source={require("../images/background.png")}
      ></Image>
      <View style={styles.navBar}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="arrow-back" size={24} color="white" />
        </TouchableOpacity>
      </View>
      <View>
        <Text style={styles.greeting}>Lets Start</Text>
      </View>
      <View style={styles.bottomContainer}>
        <Text style={styles.title}>Sign Up</Text>
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Name"
          placeholderTextColor="#C4C4C4"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Username"
          placeholderTextColor="#C4C4C4"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Phone Number"
          placeholderTextColor="#C4C4C4"
          autoCapitalize="none"
        />
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Password"
          placeholderTextColor="#C4C4C4"
          autoCapitalize="none"
          secureTextEntry={true}
        />
        <View style={styles.textContainer}>
          <Text style={styles.orangeText}>Already have an account ?</Text>
          <TouchableOpacity onPress={() => login()}>
            <Text style={styles.textLink}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  backgroundImage: {
    position: "absolute",
    top: -200,
    left: 0,
    bottom: 0,
    right: 0,
    width: "100%",
    height: "100%",
  },
  navBar: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 18,
  },
  greeting: {
    fontFamily: "Roboto",
    fontSize: 48,
    lineHeight: 56,
    color: "white",
    marginTop: 12,
    marginLeft: 34,
  },
  bottomContainer: {
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    backgroundColor: "white",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 34,
    paddingHorizontal: 34,
  },
  title: {
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 36,
    color: "#4F4F4F",
    marginBottom: 19,
  },
  input: {
    width: "100%",
    paddingVertical: 14,
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    marginTop: 19,
  },
  textContainer: {
    marginTop: 61,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  orangeText: {
    color: "#F18843",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 15,
    lineHeight: 16,
  },
  textLink: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 15,
    lineHeight: 16,
  },
});
