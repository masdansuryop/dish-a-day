import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  StatusBar,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

export default class DetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    const { route } = this.props;
    this.state = route.params;
    console.log(this.state.data);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Ionicons name="arrow-back" size={24} color="#4F4F4F" />
          </TouchableOpacity>
          <Image
            style={styles.logo}
            source={require("../images/black-logo.png")}
          ></Image>
          <TouchableOpacity>
            <Ionicons name="ellipsis-vertical" size={24} color="#4F4F4F" />
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.body}>
            <Image
              style={styles.productImage}
              source={{
                uri: this.state.data.image,
              }}
            ></Image>
            <View style={styles.descContainer}>
              <View style={styles.loveContainer}>
                <Ionicons name="heart" size={24} color="red" />
              </View>
              <Text style={styles.productName}>
                {this.state.data.title.substring(0, 20)}
              </Text>
              <Text style={styles.productDescription}>
                {this.state.data.summary.substring(0, 60)}
              </Text>
              <View style={styles.productDetails}>
                <Text style={styles.productDuration}>
                  {this.state.data.readyInMinutes} MIN
                </Text>
                <Text style={styles.productLevel}>
                  {this.state.data.dishTypes[0]}
                </Text>
              </View>
              <View style={styles.productTabBar}>
                <View>
                  <Text style={styles.tabBarText}>Ingredients</Text>
                </View>
              </View>
              <View style={styles.tabBarContent}>
                <FlatList
                  data={this.state.data.extendedIngredients}
                  renderItem={({ item }) => (
                    <View style={styles.contentItem}>
                      <Text style={styles.contentName}>{item.original}</Text>
                    </View>
                  )}
                  keyExtractor={(item) => item.id.toString()}
                  pagingEnabled
                  showsHorizontalScrollIndicator={false}
                />
              </View>
              <View style={styles.productTabBar}>
                <View>
                  <Text style={styles.tabBarText}>Direction</Text>
                </View>
              </View>
              <View style={styles.tabBarContent}>
                <View style={styles.contentItem}>
                  <Text style={styles.contentName}>
                    {this.state.data.instructions}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  navBar: {
    height: 44,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 18,
    justifyContent: "space-between",
  },
  logo: {
    width: 90.1,
    height: 24,
  },
  body: {
    marginHorizontal: 25,
  },
  productImage: {
    height: 420,
    width: 400,
    position: "absolute",
    top: 0,
    left: -25,
  },
  descContainer: {
    position: "absolute",
    top: 295,
    paddingHorizontal: 18,
    paddingVertical: 15,
    backgroundColor: "white",
    alignItems: "center",
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    width: 345,
  },
  productName: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 24,
    lineHeight: 28,
  },
  productDescription: {
    color: "#BDBDBD",
    fontFamily: "Roboto",
    fontSize: 12,
    lineHeight: 14,
    marginTop: 12,
    textAlign: "center",
  },
  loveContainer: {
    position: "absolute",
    top: -20,
    right: 22,
    backgroundColor: "white",
    borderRadius: 50,
    padding: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  productDetails: {
    flexDirection: "row",
    width: 200,
    justifyContent: "center",
    marginTop: 19,
  },
  productDuration: {
    borderRightWidth: 0.5,
    borderColor: "black",
    paddingRight: 25,
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 14,
    lineHeight: 16,
  },
  productLevel: {
    paddingLeft: 25,
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontWeight: "bold",
    fontSize: 14,
    lineHeight: 16,
  },
  productTabBar: {
    marginTop: 31,
    flexDirection: "row",
    justifyContent: "space-around",
    width: 325,
  },
  tabBarText: {
    color: "#F18843",
    fontFamily: "Roboto",
    fontWeight: "500",
    fontSize: 14,
    lineHeight: 16,
  },
  tabBarContent: {
    margin: 13,
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: "#BDBDBD",
    padding: 25,
    width: 325,
  },
  contentItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderColor: "#BDBDBD",
    paddingVertical: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: "#BDBDBD",
  },
  contentName: {
    color: "#4F4F4F",
    fontFamily: "Roboto",
    fontSize: 14,
    lineHeight: 16,
  },
  contentSize: {
    color: "#BDBDBD",
    fontFamily: "Roboto",
    fontSize: 14,
    lineHeight: 16,
  },
});
